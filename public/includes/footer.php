 <div class="footer">
        <div class="content">
            <div class="column-container">
                <div class="column">
                    <h3><a href="#">Buy</a></h3>
                    <ul>
                        <li><a href="#">Registration</a></li>
                        <li><a href="#">eBay Money Back Guarantee</a></li>
                        <li><a href="#">Bidding & buying help</a></li>
                        <li><a href="#">Stores</a></li>
                        <li><a href="#">eBay Local</a></li>
                        <li><a href="#">eBay guides</a></li>
                    </ul>
                    <h3><a href="#">Gift center</a></h3>
                    <ul>
                        <li><a href="#">eBay Wish list</a></li>
                        <li><a href="#">Gift cards</a></li>
                        <li><a href="#">Gift guide</a></li>
                        <li><a href="#">Group gifts</a></li>
                    </ul>
                </div>
                <div class="column">
                    <h3><a href="#">Sell</a></h3>
                    <ul>
                        <li><a href="#">Start selling</a></li>
                        <li><a href="#">Learn to sell</a></li>
                        <li><a href="#">Business sellers</a></li>
                        <li><a href="#">Affiliates</a></li>
                
                    </ul>
                    <h3><a href="#">Tools & apps</a></h3>
                    <ul>
                        <li><a href="#">Mobile apps</a></li>
                        <li><a href="#">Downloads</a></li>
                        <li><a href="#">Developers</a></li>
                        <li><a href="#">Security center</a></li>
                        <li><a href="#">eBay official time</a></li>
                        <li><a href="#">Site map</a></li>       
                    </ul>                    
                </div>
                <div class="column">
                    <h3><a href="#">eBay companies</a></h3>
                    <ul>
                        <li><a href="#">eBay Classifieds</a></li>
                        <li><a href="#">Shopping.com</a></li>
                        <li><a href="#">Half.com</a></li>
                        <li><a href="#">PayPal</a></li>
                        <li><a href="#">StubHub</a></li>
                        <li><a href="#">See all companies</a></li>
                    </ul>
                    <h3><a href="#">Stay connected</a></h3>
                    <ul>
                        <li><a href="#">eBay's Blogs</a></li>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Google+</a></li>
                    </ul>                    
                </div>
                <div class="column">
                    <h3><a href="#">About eBay</a></h3>
                    <ul>
                        <li><a href="#">Company info</a></li>
                        <li><a href="#">Investors</a></li>
                        <li><a href="#">2014 Annual Shareholders' Meeting</a></li>
                        <li><a href="#">News</a></li>
                        <li><a href="#">eBay Inc blog</a></li>
                        <li><a href="#">Government relations</a></li>
                        <li><a href="#">Jobs</a></li>
                        <li><a href="#">Advertise with us</a></li>
                        <li><a href="#">Policies</a></li>
                        <li><a href="#">Verified Rights Owner (VeRO) Program</a></li>
                        <li><a href="#">Tell us what you think</a></li>
                    </ul>                    
                </div>
                <div class="column">
                    <h3><a href="#">Help & Contact</a></h3>
                    <ul>
                        <li><a href="#">Resolution Center</a></li>
                        <li><a href="#">Seller Information Center</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                    <h3><a href="#">Community</a></h3>
                    <ul>
                        <li><a href="#">Announcements</a></li>
                        <li><a href="#">Answer center</a></li>
                        <li><a href="#">Discussion boards</a></li>
                        <li><a href="#">eBay Giving Works</a></li>
                        <li><a href="#">eBay Celebrity</a></li>
                        <li><a href="#">Groups</a></li>
                        <li><a href="#">eBay top shared</a></li>
                    </ul>
                </div><!-- end .column -->
            </div> <!-- end .column-container -->
            <div class="legal">
                <a href="#"><img src="/assets/images/money-back-guarantee.png" /></a>
                <p>Copyright &copy; 1995-<?php print $thisYear; ?> eBay Inc. All Rights Reserved. <a href="#">User Agreement</a>, <a href="#">Privacy</a> and <a href="#">Cookies</a>.</p>
            </div> <!-- end .legal -->
        </div> <!-- end .content -->
    </div> <!-- .footer -->
</body>
</html>