<?php  include "includes/header.php"; ?>
    <div class="main">
        <div class="content">
            <div class="featured-collection">
                <div class="row clear-fix">
                    <div class="featured-item">
                    <div class="item-images clear-fix">
                        <div class="main-image">
                            <img src="/assets/images/hipster-wagon.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                        </div>
                        <div class="secondary-images">
                            <div class="secondary-image">
                               <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" /> 
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                        </div>
                    </div>
                    <div class="text clear-fix">
                        <div class="main-text">
                            <h2><a href="#">Use the force</a></h2>
                            <p>Shop for stuff of Star Wars</p>
                        </div>
                        <div class="sidebar-text">
                            <ul>
                                <li>17 items</li>
                                <li>$5-up</li>
                                <li><a href="#">Shop Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                    <div class="featured-item">
                    <div class="item-images clear-fix">
                        <div class="main-image">
                            <img src="/assets/images/hipster-wagon.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                        </div>
                        <div class="secondary-images">
                            <div class="secondary-image">
                               <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" /> 
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                        </div>
                    </div>
                    <div class="text clear-fix">
                        <div class="main-text">
                            <h2><a href="#">Use the force</a></h2>
                            <p>Shop for stuff of Star Wars</p>
                        </div>
                        <div class="sidebar-text">
                            <ul>
                                <li>17 items</li>
                                <li>$5-up</li>
                                <li><a href="#">Shop Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
                <div class="row clear-fix">
                    <div class="featured-item">
                    <div class="item-images clear-fix">
                        <div class="main-image">
                            <img src="/assets/images/hipster-wagon.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                        </div>
                        <div class="secondary-images">
                            <div class="secondary-image">
                               <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" /> 
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                        </div>
                    </div>
                    <div class="text clear-fix">
                        <div class="main-text">
                            <h2><a href="#">Use the force</a></h2>
                            <p>Shop for stuff of Star Wars</p>
                        </div>
                        <div class="sidebar-text">
                            <ul>
                                <li>17 items</li>
                                <li>$5-up</li>
                                <li><a href="#">Shop Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                    <div class="featured-item">
                    <div class="item-images clear-fix">
                        <div class="main-image">
                            <img src="/assets/images/hipster-wagon.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                        </div>
                        <div class="secondary-images">
                            <div class="secondary-image">
                               <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" /> 
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                            <div class="secondary-image">
                                <img src="/assets/images/baby-toy.jpg" alt="The hippiest wagon of all" title="Hipster wagon" />
                            </div>
                        </div>
                    </div>
                    <div class="text clear-fix">
                        <div class="main-text">
                            <h2><a href="#">Use the force</a></h2>
                            <p>Shop for stuff of Star Wars</p>
                        </div>
                        <div class="sidebar-text">
                            <ul>
                                <li>17 items</li>
                                <li>$5-up</li>
                                <li><a href="#">Shop Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>

            </div>
        </div>
    </div>
<?php  include "includes/footer.php"; ?>